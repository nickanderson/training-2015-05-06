#!/bin/sh
# Installing Neil Watson's CFEngine 3 syntax highlighter
cat <<EOF >> ~/.vimrc

filetype plugin on
syntax enable

" Set tab stops for good style
setlocal tabstop=4
setlocal softtabstop=4
setlocal shiftwidth=4
setlocal smarttab
setlocal expandtab

au BufRead,BufNewFile *.cf set ft=cf3
" Disable folding so it does not confuse students not familiar with it
set nofoldenable 
" disable abbreviations so we do not confuse students
let g:DisableCFE3KeywordAbbreviations=0

" functions Eatchar and Getchar
fun! Getchar()
  let c = getchar()
  if c != 0
    let c = nr2char(c)
  endif
  return c
endfun

fun! Eatchar(pat)
  let c = Getchar()
  return (c =~ a:pat) ? '' : c
endfun

EOF

echo Install syntax highlighter and editor plugin

yum -y install vim-enhanced
mkdir -p ~/.vim/ftplugin  ~/.vim/syntax 

wget --no-check-certificate -O ~/.vim/syntax/cf3.vim \
      https://github.com/neilhwatson/vim_cf3/raw/master/syntax/cf3.vim

wget --no-check-certificate -O ~/.vim/ftplugin/cf3.vim \
      https://github.com/neilhwatson/vim_cf3/raw/master/ftplugin/cf3.vim
